<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'cyberhull_geopopup/popup'
 */
$table = $installer->getConnection()
        ->newTable($installer->getTable('cyberhull_geopopup/popup'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Popup ID')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'Popup Name')
        ->addColumn('country', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
            'nullable' => false,
                ), 'Country')
        ->addColumn('state', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
            'nullable' => false,
                ), 'State')
        ->addColumn('cms_block_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => false,
                ), 'CMS Block Identifier')
        ->addColumn('cookie_lifetime', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Cookie lifetime in minutes')
        ->addColumn('target_page', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Target Page')
        ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'Sort Order')
        ->setComment('Geo Popup Table');

$installer->getConnection()->createTable($table);

/**
 * Create table 'cyberhull_geopopup/popup_store'
 */
$table = $installer->getConnection()
        ->newTable($installer->getTable('cyberhull_geopopup/popup_store'))
        ->addColumn('popup_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'primary' => true,
                ), 'Popup ID')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'primary' => true,
                ), 'Store Id')
        ->setComment('Geo Popup Store Table');

$installer->getConnection()->createTable($table);

/**
 * Add foreign keys
 */
$installer->getConnection()->addForeignKey(
        $installer->getFkName('cyberhull_geopopup/popup_store', 'popup_id', 'cyberhull_geopopup/popup', 'entity_id'), 
        $installer->getTable('cyberhull_geopopup/popup_store'), 'popup_id', 
        $installer->getTable('cyberhull_geopopup/popup'), 'entity_id'
);

$installer->endSetup();
