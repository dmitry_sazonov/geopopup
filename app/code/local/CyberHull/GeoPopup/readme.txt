/**
 * Cyberhull_GeoPopup
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */

This extension implement geo targeted popups.
Geo data based on IP address.
It used CMS blocks for popup content.

This module requires jQuery and FancyBox JavaScript library. 
Which may be included in Admin Panel -> System -> Configuration -> Geo Popup

For some IPs the module can't determine geo data in this case popup will not be displayed 

Usage:
1. Create CMS block with the necessary information
2. Create new popup (Admin Panel -> CMS -> Popups -> Add New Popup)
3. Select required Countries and/or States 
4. Select target page. Specific CMS page or all pages

This product includes GeoLite data created by MaxMind, available from http://www.maxmind.com.