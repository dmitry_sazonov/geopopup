<?php
/**
 * Geopopup resoure model
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Model_Resource_Popup extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Init resource model
     */
    public function _construct()
    {
        $this->_init('cyberhull_geopopup/popup', 'entity_id');
    }

    /**
     * Perform operations after object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return CyberHull_GeoPopup_Model_Resource_Popup
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array) $object->getStores();

        $table = $this->getTable('cyberhull_geopopup/popup_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);

        if ($delete) {
            $where = array(
                'popup_id = ?' => (int) $object->getId(),
                'store_id IN (?)' => $delete
            );

            $this->_getWriteAdapter()->delete($table, $where);
        }

        if ($insert) {
            $data = array();

            foreach ($insert as $storeId) {
                $data[] = array(
                    'popup_id' => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }

        return parent::_afterSave($object);
    }

    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return CyberHull_GeoPopup_Model_Resource_Popup
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
            $object->setData('stores', $stores);
            $object->setCountry(@unserialize($object->getCountry()));
            $object->setState(@unserialize($object->getState()));
        }

        return parent::_afterLoad($object);
    }

    /**
     * Perform actions before object save
     *
     * @param Varien_Object $object
     * @return CyberHull_GeoPopup_Model_Resource_Popup
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $object->setCountry(serialize($object->getCountry()));
        $object->setState(serialize($object->getState()));
        return $this;
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select = $adapter->select()
                ->from($this->getTable('cyberhull_geopopup/popup_store'), 'store_id')
                ->where('popup_id = :popup_id');

        $binds = array(
            ':popup_id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param CyberHull_GeoPopup_Model_Resource_Popup $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {
            $stores = array(
                (int) $object->getStoreId(),
                Mage_Core_Model_App::ADMIN_STORE_ID,
            );

            $select->join(array('cps' => $this->getTable('cyberhull_geopopup/popup_store')), 
                        $this->getMainTable() . '.entity_id = cps.block_id', array('store_id'))
                    ->where('is_active = ?', 1)
                    ->where('cps.store_id in (?) ', $stores)
                    ->order('store_id DESC')
                    ->limit(1);
        }
        return $select;
    }

}
