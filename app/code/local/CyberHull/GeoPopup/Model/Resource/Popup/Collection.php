<?php
/**
 * Geopopup collection
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Model_Resource_Popup_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Init collection
     */
    public function _construct()
    {
        $this->_init('cyberhull_geopopup/popup');
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    /**
     * Add filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return CyberHull_GeoPopup_Model_Resource_Popup_Collection
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }

        if (!is_array($store)) {
            $store = array($store);
        }

        if ($withAdmin) {
            $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
        }

        $this->addFilter('store', array('in' => $store), 'public');

        return $this;
    }

    /**
     * Join store relation table if there is store filter
     */
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('store')) {
            $this->getSelect()->join(
                    array('store_table' => $this->getTable('cyberhull_geopopup/popup_store')), 
                    'main_table.entity_id = store_table.popup_id', array()
            )->group('main_table.entity_id');

            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }

    /**
     * Add filter for CMS block title
     * 
     * @param string $title
     */
    public function addCmsBlockTitleFilter($title)
    {
        $this->addFieldToFilter('cms_block_table.title', array('like' => '%' . $title . '%'));
    }
    /**
     * Add CMS block title
     * 
     * @return CyberHull_GeoPopup_Model_Resource_Popup_Collection
     */
    public function addBlockTitle()
    {
        $this->getSelect()->join(
                array('cms_block_table' => $this->getTable('cms/block')), 
                'main_table.cms_block_id = cms_block_table.block_id', 
                array('cms_block_title' => 'title'));
        return $this;
    }

    /**
     * update data for each collection item when collection was loaded
     *
     * @return CyberHull_GeoPopup_Model_Resource_Popup_Collection
     */
    public function _afterLoad()
    {
        parent::_afterLoad();
        foreach ($this->_items as $item) {
            $item->afterLoad();
        }

        return $this;
    }

}
