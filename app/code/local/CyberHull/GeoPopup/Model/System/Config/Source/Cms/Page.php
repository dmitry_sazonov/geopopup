<?php
/**
 * CMS pages source model
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Model_System_Config_Source_Cms_Page
{

    /**
     * Cached options
     * 
     * @var array 
     */
    protected $_options;

    /**
     * Get CMS pages list
     * 
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $collection = Mage::getResourceModel('cms/page_collection');
            $this->_options[] = array(
                'label' => Mage::helper('cyberhull_geopopup')->__('All pages'),
                'value' => '',
            );
            foreach ($collection as $page) {
                $this->_options[] = array(
                    'label' => $page->getTitle(),
                    'value' => $page->getId(),
                );
            }
        }
        return $this->_options;
    }

}
