<?php
/**
 * CMS blocks source model
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Model_System_Config_Source_Cms_Block
{

    /**
     * Cached options
     * 
     * @var array 
     */
    protected $_options;

    /**
     * Get CMS blocks list
     * 
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $collection = Mage::getResourceModel('cms/block_collection');
            $this->_options[] = array(
                'label' => Mage::helper('cyberhull_geopopup')->__('Please select CMS Block...'),
                'value' => '',
            );
            foreach ($collection as $block) {
                $this->_options[] = array(
                    'label' => $block->getTitle(),
                    'value' => $block->getId(),
                );
            }
        }
        return $this->_options;
    }

}
