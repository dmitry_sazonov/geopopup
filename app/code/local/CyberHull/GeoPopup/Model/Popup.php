<?php
/**
 * Geopopup resoure model
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Model_Popup extends Mage_Core_Model_Abstract
{

    /**
     * Used for addition state check
     */
    const US_COUNTRY_CODE = 'US';

    /**
     * Loaded CMS block
     * 
     * @var Mage_Cms_Model_Block 
     */
    protected $_cmsBlock;

    /**
     * Init model
     */
    public function _construct()
    {
        $this->_init('cyberhull_geopopup/popup');
    }

    /**
     * Is popup avaliable for page
     * 
     * @return boolean
     */
    public function isAvailable()
    {

        $regionInfo = Mage::helper('cyberhull_geopopup')->getInfo();
        if (!$regionInfo->getCountryCode()) {
            return false;
        }
        if (!in_array($regionInfo->getCountryCode(), $this->getCountry())) {
            return false;
        }
        if ($regionInfo->getCountryCode() == self::US_COUNTRY_CODE) {
            if (!in_array($regionInfo->getRegion(), $this->getState())) {
                return false;
            }
        }
        if (!$this->_getBlock()) {
            return false;
        }
        return true;
    }

    /**
     * Load CMS block
     * 
     * @return boolean|Mage_Cms_Model_Block
     */
    protected function _getBlock()
    {
        $storeId = Mage::app()->getStore()->getId();
        if ($this->_cmsBlock === null) {
            $block = Mage::getModel('cms/block')->getCollection()
                    ->addStoreFilter($storeId)
                    ->addFieldToFilter('is_active', '1')
                    ->addFieldToFilter('main_table.block_id', $this->getCmsBlockId())
                    ->getFirstItem();
            if ($block->getId()) {
                $this->_cmsBlock = $block;
            }
            else {
                $this->_cmsBlock = false;
            }
        }
        return $this->_cmsBlock;
    }

    /**
     * Get CMS Block content
     * 
     * @return string
     */
    public function getContent()
    {
        if ($block = $this->_getBlock()) {
            return $block->getContent();
        }
        return null;
    }

}
