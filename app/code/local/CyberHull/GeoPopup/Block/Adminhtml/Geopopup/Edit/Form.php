<?php
/**
 * Geopopup edit form
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Block_Adminhtml_Geopopup_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('geopopup_form');
        $this->setTitle(Mage::helper('cms')->__('Popup Information'));
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return CyberHull_GeoPopup_Block_Adminhtml_Geopopup_Edit_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('geopopup');

        $form = new Varien_Data_Form(
                array('id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post')
        );

        $form->setHtmlIdPrefix('popup_');

        $fieldset = $form->addFieldset('base_fieldset', 
                array('legend' => Mage::helper('cyberhull_geopopup')->__('General Information')));

        if ($model->getId()) {
            $fieldset->addField('entity_id', 'hidden', array(
                'name' => 'entity_id',
            ));
        }

        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => Mage::helper('cyberhull_geopopup')->__('Popup Name'),
            'title' => Mage::helper('cyberhull_geopopup')->__('Popup Name'),
            'required' => true,
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'multiselect', array(
                'name' => 'stores[]',
                'label' => Mage::helper('cms')->__('Store View'),
                'title' => Mage::helper('cms')->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()
                    ->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('country', 'multiselect', array(
            'name' => 'country',
            'label' => Mage::helper('cyberhull_geopopup')->__('Country'),
            'values' => Mage::helper('cyberhull_geopopup')->getCounties(),
                )
        );
        $fieldset->addField('state', 'multiselect', array(
            'name' => 'state',
            'label' => Mage::helper('cyberhull_geopopup')->__('State (For US only)'),
            'values' => Mage::helper('cyberhull_geopopup')->getStates(),
                )
        );
        $fieldset->addField('cms_block_id', 'select', array(
            'name' => 'cms_block_id',
            'label' => Mage::helper('cyberhull_geopopup')->__('CMS Block'),
            'values' => Mage::getModel('cyberhull_geopopup/system_config_source_cms_block')->toOptionArray(),
            'required' => true,
                )
        );

        $fieldset->addField('target_page', 'select', array(
            'name' => 'target_page',
            'label' => Mage::helper('cyberhull_geopopup')->__('Target Page'),
            'values' => Mage::getModel('cyberhull_geopopup/system_config_source_cms_page')->toOptionArray(),
                )
        );

        $fieldset->addField('cookie_lifetime', 'text', array(
            'name' => 'cookie_lifetime',
            'label' => Mage::helper('cyberhull_geopopup')->__('Cookie Lifetime'),
            'title' => Mage::helper('cyberhull_geopopup')->__('Cookie Lifetime'),
            'required' => false,
        ));

        $fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => Mage::helper('cyberhull_geopopup')->__('Sort Order'),
            'title' => Mage::helper('cyberhull_geopopup')->__('Sort Order'),
            'required' => false,
        ));

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
