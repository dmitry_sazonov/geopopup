<?php
/**
 * Geopopup edit block
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Block_Adminhtml_Geopopup_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * Initialize form
     */
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_geopopup';
        $this->_blockGroup = 'cyberhull_geopopup';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('cyberhull_geopopup')->__('Save Popup'));
        $this->_updateButton('delete', 'label', Mage::helper('cyberhull_geopopup')->__('Delete Popup'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('geopopup')->getId()) {
            return Mage::helper('cyberhull_geopopup')
                    ->__("Edit Popup '%s'", $this->escapeHtml(Mage::registry('geopopup')->getName()));
        }
        else {
            return Mage::helper('cyberhull_geopopup')->__('New Popup');
        }
    }

}
