<?php
/**
 * Geopopup grid
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Block_Adminhtml_Geopopup_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Initialize grid
     * Set sort settings
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('popup_grid');
        $this->setDefaultSort('sort_order');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Set collection
     *
     * @return CyberHull_GeoPopup_Block_Adminhtml_Geopopup_Grid
     */
    protected function _prepareCollection()
    {
        /** @var $collection Mage_SalesRule_Model_Mysql4_Rule_Collection */
        $collection = Mage::getModel('cyberhull_geopopup/popup')
                ->getResourceCollection();
        $collection->addBlockTitle();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * Add grid columns
     *
     * @return CyberHull_GeoPopup_Block_Adminhtml_Geopopup_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('cyberhull_geopopup')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'entity_id',
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('cyberhull_geopopup')->__('Popup Name'),
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('cms_block_title', array(
            'header' => Mage::helper('cyberhull_geopopup')->__('Cms Block'),
            'align' => 'left',
            'width' => '200px',
            'index' => 'cms_block_title',
            'filter_condition_callback'
            => array($this, '_filterCmsBlockTitleCondition'),
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => Mage::helper('cms')->__('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'sortable' => false,
                'filter_condition_callback'
                => array($this, '_filterStoreCondition'),
            ));
        }

        $this->addColumn('sort_order', array(
            'header' => Mage::helper('cyberhull_geopopup')->__('Priority'),
            'align' => 'right',
            'index' => 'sort_order',
            'width' => '100px',
        ));

        parent::_prepareColumns();
        return $this;
    }

    /**
     * Retrieve row click URL
     *
     * @param Varien_Object $row
     * 
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getEntityId()));
    }

    /**
     * Store filter
     *
     * @param CyberHull_GeoPopup_Model_Resource_Popup_Collection $row
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $this->getCollection()->addStoreFilter($value);
    }

    /**
     * Add store to collection elements
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

}
