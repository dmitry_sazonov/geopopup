<?php
/**
 * Geopopup content block
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Block_Adminhtml_Geopopup extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * Initialize block
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_geopopup';
        $this->_blockGroup = 'cyberhull_geopopup';
        $this->_headerText = Mage::helper('cyberhull_geopopup')->__('Popups');
        $this->_addButtonLabel = Mage::helper('cyberhull_geopopup')->__('Add New Popup');
        parent::__construct();
    }

}
