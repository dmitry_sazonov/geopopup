<?php
/**
 * Geopopup frontend block
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Block_Popup extends Mage_Core_Block_Template
{

    /**
     * Current popup
     *
     * @var CyberHull_GeoPopup_Model_Popup
     */
    protected $_popup;

    /**
     * Cookie prefix.
     * Used as part of cookie name
     */
    const COOKIE_PREFIX = 'geopopup_';

    /**
     * Load current popup
     *
     * @return boolean|CyberHull_GeoPopup_Model_Popup
     */
    protected function _getPopup()
    {
        if ($this->_popup === null) {
            $storeId = Mage::app()->getStore()->getId();

            $collection = Mage::getModel('cyberhull_geopopup/popup')->getCollection();
            $collection->setOrder('sort_order');
            $collection->addStoreFilter($storeId);

            foreach ($collection as $popup) {
                if ($popup->isAvailable()) {
                    $this->_popup = $popup;
                    return $this->_popup;
                }
            }
            $this->_popup = false;
            return $this->_popup;
        }
    }

    /**
     * Get current popup
     *
     * @return null|CyberHull_GeoPopup_Model_Popup
     */
    public function getPopup()
    {
        if (!Mage::helper('cyberhull_geopopup')->getIsEnabled()) {
            return null;
        }
        if (!$popup = $this->_getPopup()) {
            return null;
        }
        if ($popup->getTargetPage() && $popup->getTargetPage() != Mage::getSingleton('cms/page')->getId()) {
            return null;
        }
        $cookieId = self::COOKIE_PREFIX . $popup->getId();
        if (Mage::getModel('core/cookie')->get($cookieId)) {
            return null;
        }
        $lifetime = $popup->getCookieLifetime() * 60;
        if ($popup->getCookieLifetime()) {
            Mage::getModel('core/cookie')->set($cookieId, '1', $lifetime);
        }
        return $popup;
    }

    /**
     * Get cache key informative items
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $key = parent::getCacheKeyInfo();
        if ($popup = $this->getPopup()) {
            $key['popup_id'] = $popup->getId();
        }
    }

}
