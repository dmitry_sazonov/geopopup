<?php
/**
 * Geopopup helper
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Region Names array
     *
     * @var array
     */
    protected $_geoipRegionName;

    /**
     * Geo info
     *
     * @var Varien_Object
     */
    protected $_info;

    /**
     * Is module enabled
     *
     * @return int
     */
    public function getIsEnabled()
    {
        return Mage::getStoreConfig('geopopup/settings/is_enabled');
    }

    /**
     * Init MaxMind GeoIP
     *
     * @return GeoIp
     */
    protected function _openDb()
    {
        if (!function_exists("geoip_open")) {
            include(Mage::getBaseDir('lib') . "/MaxMind/GeoIP/geoipcity.php");
            $this->_geoipRegionName = $GEOIP_REGION_NAME;
        }
        $gi = geoip_open(Mage::getBaseDir('lib') . "/MaxMind/GeoIP/data/GeoLiteCity.dat", GEOIP_STANDARD);
        return $gi;
    }

    /**
     * Get geo information by IP
     * 
     * @param string $ip
     * @return array
     */
    public function getInfoByIp($ip)
    {
        $gi = $this->_openDb();
        $result = (array) GeoIP_record_by_addr($gi, $ip);
        return $result;
    }

    /**
     * Get geo information for current user
     * 
     * @return Varien_Object
     */
    public function getInfo()
    {
        if (!$this->_info) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $this->_info = new Varien_Object();
            $this->_info->setData($this->getInfoByIp($ip));
        }
        return $this->_info;
    }

    /**
     * Get counties array
     * 
     * @return array
     */
    public function getCounties()
    {
        $gi = $this->_openDb();
        $countries = array();
        $geoipCounties = $gi->GEOIP_COUNTRY_NAMES;
        $geoipCountryCodes = $gi->GEOIP_COUNTRY_CODES;
        asort($geoipCounties);
        foreach ($geoipCounties as $id => $countyName) {
            if ($countyName && isset($geoipCountryCodes[$id]) && $geoipCountryCodes[$id]) {
                $countries[] = array('value' => $geoipCountryCodes[$id], 'label' => $countyName);
            }
        }
        return $countries;
    }

    /**
     * Get States array
     * 
     * @return array
     */
    public function getStates()
    {
        $gi = $this->_openDb();
        $states = array();
        foreach ($this->_geoipRegionName['US'] as $stateId => $state) {
            $states[] = array('value' => $stateId, 'label' => $state);
        }
        return $states;
    }

}
