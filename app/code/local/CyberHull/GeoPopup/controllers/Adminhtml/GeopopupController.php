<?php
/**
 * Geopopup adminhtml controller
 *
 * @category    Cyberhull
 * @package     Cyberhull_GeoPopup
 * @author      Dmitry Sazonov <dmitry.sazonov@cyberhull.com>
 */
class CyberHull_GeoPopup_Adminhtml_GeopopupController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Init actions
     *
     * @return CyberHull_GeoPopup_Adminhtml_GeopopupController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
                ->_setActiveMenu('cms/popup')
                ->_addBreadcrumb(Mage::helper('cms')->__('CMS'), Mage::helper('cms')->__('CMS'))
                ->_addBreadcrumb(Mage::helper('cyberhull_geopopup')
                        ->__('Popups'), Mage::helper('cyberhull_geopopup')->__('Popups'))
        ;
        return $this;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Geo Popups'));
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Create new popup
     */
    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    /**
     * Edit popup
     */
    public function editAction()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Popups'));

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('cyberhull_geopopup/popup');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')
                        ->addError(Mage::helper('cyberhull_geopopup')->__('This popup no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getTitle() : $this->__('New Popup'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        Mage::register('geopopup', $model);

        // 5. Build edit form
        if ($id) {
            $breadcrumb = Mage::helper('cyberhull_geopopup')->__('Edit Popup');
        }
        else {
            $breadcrumb = Mage::helper('cyberhull_geopopup')->__('New Popup');
        }

        $this->_initAction()
                ->_addBreadcrumb($breadcrumb, $breadcrumb)
                ->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('entity_id');
            $model = Mage::getModel('cyberhull_geopopup/popup')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')
                        ->addError(Mage::helper('cms')->__('This popup no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            // init model and set data

            $model->setData($data);

            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                Mage::getSingleton('adminhtml/session')
                        ->addSuccess(Mage::helper('cms')->__('The popup has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('cyberhull_geopopup/popup');
                $model->load($id);
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')
                        ->addSuccess(Mage::helper('cms')->__('The popup has been deleted.'));
                // go to grid
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('cms')->__('Unable to find a popup to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/geopopup');
    }

}
